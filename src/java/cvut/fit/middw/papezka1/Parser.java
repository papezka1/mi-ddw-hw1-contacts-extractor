package cvut.fit.middw.papezka1;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author papezka1
 */
public class Parser {
    public String parseText(String url) throws IOException{
        Document doc = Jsoup.connect(url).get();
        Element body = doc.body();
        
        return body.text();
    }    
}
