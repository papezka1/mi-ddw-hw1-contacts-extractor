package cvut.fit.middw.papezka1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author papezka1
 */
@WebServlet(name = "Servlet", urlPatterns = {"/extract"})
public class Servlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("url", "");
        request.setAttribute("emails", "");
        request.setAttribute("phones", "");
        request.setAttribute("emails_cnt", "0");
        request.setAttribute("phones_cnt", "0");
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String url = request.getParameter("url");
                
        Parser parser = new Parser();
        String pageContent = parser.parseText(url);
        
        GateClient gateClient = new GateClient(pageContent);
        gateClient.run();
        
        ArrayList<String> emailList = gateClient.getEmails();
        ArrayList<String> phoneList = gateClient.getPhones();

        String emails = "";
        String phones = "";
        
        ListIterator<String> emailsIterator = emailList.listIterator();
        while (emailsIterator.hasNext()) {
            emails += "<li>" + emailsIterator.next() + "</li>";
        }
        
        ListIterator<String> phonesIterator = phoneList.listIterator();
        while (phonesIterator.hasNext()) {
            phones += "<li>" + phonesIterator.next() + "</li>";
        }
        
        request.setAttribute("url", url);
        request.setAttribute("emails", emails);
        request.setAttribute("phones", phones);
        request.setAttribute("emails_cnt", emailList.size());
        request.setAttribute("phones_cnt", phoneList.size());        
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
