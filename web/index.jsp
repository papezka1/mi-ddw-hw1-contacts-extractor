<%-- 
    Document   : index
    Created on : 24.3.2015, 20:27:20
    Author     : papezka1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contacts extractor</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
            <style type="text/css">
                body {
                    margin: 3em;
                }
                #urlform {
                    width: 30em;
                }
                #options {
                    padding-left: 1em;
                }
                #emails, #phones {
                    list-style-type: none;
                    padding: 0 0 1em 1em;
                }
                #description{
                    color: gray;
                }
            </style>
    </head>
    <body>
        <div class="page-header">
            <h1>Contacts extractor <small>mi-mdw hw1</small></h1>
        </div>
        <div class="row">
            <div class="col-md-6">
                <form id="urlform" action="extract" method="post">
                    <div class="input-group">
                        <input type="text" name="url" class="form-control" placeholder="Enter web page URL" value="${url}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-success">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>            
                    </div>
                    <div id="options">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" checked> email addresses
                          </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" checked> phone numbers
                          </label>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" disabled> names
                          </label>
                        </div>  
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" disabled> addresses
                          </label>
                        </div>
                    </div>    
                </form>
                <br>
                
                <h4>Email addresses <span class="badge">${emails_cnt}</span></h4>
                <ul id="emails">
                    ${emails}
                </ul>
                
                <h4>Phone numbers <span class="badge">${phones_cnt}</span></h4>
                <ul id="phones">
                    ${phones}
                </ul>
            </div>
            <div class="col-md-5" id="description">
                <p><em>
                    <h3>Popis</h3>
                    Contacts extractor je webová aplikace, která uživateli umožní získat kontaktní informace, jako emailové adresy, telefonní čísla, jména apod., z jiné webové stránky na základě URL adresy.
                </em></p>
            </div>
        </div>       
    </body>
</html>
